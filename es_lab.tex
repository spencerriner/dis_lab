\documentclass[12pt]{article}
\setlength\parindent{0pt}

\usepackage{url}
\usepackage{booktabs}
\usepackage{graphicx}
\usepackage{hyperref}

\usepackage[margin=.8in]{geometry}


\title{
    {CSCI 5533 - Distributed Information Systems}\\
    {Distributed Clusters in Elasticsearch}\\
    {University of Houston - Clear Lake}
}
\author{Spencer Riner}
\date{January 26, 2020}

\begin{document}
\pagenumbering{gobble}
\maketitle
\newpage
\pagenumbering{arabic}

\tableofcontents
\newpage

\section{Introduction}

Elasticsearch is a distributed and open source search engine used to index various types of unstructured data \cite{eswhatises}. Each independent machine running an instance of Elasticsearch is referred to in this lab as a \textbf{server}. Many servers who coordinate with one another comprise a \textbf{cluster}. In this lab, you will create a three-node cluster that is able to recover from an unexpected outage without any user intervention. The Elasticsearch documentation is extensive and will prove useful during the lab. You can find it here: \url{https://www.elastic.co/guide/en/elasticsearch/reference/current/index.html}

\subsection{Indices}

The essential components of an Elasticsearch cluster are \textbf{indices}, \textbf{shards}, and \textbf{replicas}. Indices are a logical namespace which map to one or more primary shards and have zero or more replica shards \cite{eswhatisindex}. An index, in some cases, can be thought of similarly to a relational database. See the table below for a correlation of the terms used in each. The advantage that Elasticsearch provides is in the sharding of indices. Depending on your architecture, shards can be distributed across multiple servers and replicated. 

\begin{table}[h!]
    \begin{center}
        \caption{\textbf{Comparison between Elasticsearch and relational database terms.}}
        \label{tab:esdbms}
        \begin{tabular}{l|l} % Alignment
            \toprule
            \textbf{Structured DBMS} & \textbf{Elasticsearch}\\
            \midrule
            Database & Index\\
            Tables & Types\\
            Rows & Documents\\
            Attributes & Properties\\
            \bottomrule
        \end{tabular}
    \end{center}
\end{table}

\begin{figure}[h!]
 \centering
 \includegraphics[width=.6\linewidth]{nestednode.png}
 \caption{\textbf{The nested structure of documents in an Elasticsearch index.}}
 \label{fig:nested}
\end{figure}


\subsection{Shards and Replicas}

The concept of shards and replicas is important to understand. A shard is a self-contained index that contains a subset of that index's documents \cite{esresilience}. This is how Elasticsearch distributes its data across multiple physical nodes. There are two types of shards: \textbf{primary} and \textbf{replica}. 

Primary shards are the original copy of the shard. Primary shards are then copied to other machines as replicas. When a server is powered off or loses its network connection, the replica shards are used to create new primary shards to ensure the availability of the index data.

In this lab, each index will have three primary shards (one for each node) and one replica of each of the primary shards. See Figure \ref{fig:cluster}.

\begin{figure}[h!]
 \includegraphics[width=\linewidth]{shard-diagram.png}
 \caption{\textbf{An Elasticsearch cluster with three nodes.}}
 \label{fig:cluster}
\end{figure}

%\newpage

The squares labelled \textbf{P0}, \textbf{P1}, and \textbf{P2} all represent primary shards. These are the original copies of the documents contained in the index. The squares labelled \textbf{R0}, \textbf{R1}, and \textbf{R2} are the respective replicas of each primary shard. You can see that if any one of the nodes were to go down unexpectedly, any of the shards could be reliably replicated to recreate the 0 through 2 shards.



\subsubsection{A Simple Shard Example}

A car dealership has decided to use Elasticsearch to manage their inventory. An \textbf{index} called \texttt{car} is created. Within the car index, there exists a \textbf{type} for each car manufacturer. Within each type, there are multiple \textbf{documents} that represent the available models. The documents have \textbf{properties} such as VIN number, license plate number, and color. 

Similar indices for trucks, SUVs, and vans may also be created. After the indices have been created and populated, \textbf{shards} and \textbf{replicas} are created according to the configuration set in the cluster. In this example, each index has three shards and one replica of each shard. The cluster has three nodes to distribute the shards across. See Figure \ref{fig:example}. 
\newpage
\begin{figure}[h!]
 \includegraphics[width=\linewidth]{shard_example2.png}
 \caption{\textbf{The \texttt{car} index distributed across three nodes.}}
 \label{fig:example}
\end{figure}

Each shard contains its own set of documents from the index, in this case multiple models of cars available at the dealership. It's important to know that the shards do not overlap, and each shard is required for the complete index. Because of the replicas, any of the three nodes could go down unexpectedly and the entire index (via Shards 1, 2, and 3) would still be available. This is one of the advantages to using a multi-node cluster with Elasticsearch.

\subsection{Node Roles}

There are many types of Elasticsearch nodes, but we are only going to use two: \textbf{Master-eligible nodes} and \textbf{Data nodes}. 

Master-eligible nodes are responsible for index creation and deletion, as well as deciding which shards are allocated to which servers \cite{esnode}. In the case of an unexpected outage on a Master node, the cluster has an election process that will designate a new master from the remaining master-eligible nodes. This presents the potential for split brain \cite{splitbrain} within the cluster. Because of this, a special role called ``Voting-only master-eligible'' exists. This terminology can be confusing, because the node is actually \textit{not} eligible to become the master, but exists only to resolve election conflicts. See the Elasticsearch docs for more info.

Data nodes store the shards and perform resource-intensive operations requested by the Master node. 

\subsection{RESTful APIs and curl}

Elasticsearch has a RESTful API available for performing create, read, update, and delete (CRUD) actions on its documents. REST stands for Representational State Transfer \cite{rest} and is easily accessed using the command line tool \texttt{curl}. Curl does Internet transfers for resources specified as URLs using Internet protocols \cite{curl} and will be the primary method of creating and deleting Elasticsearch indices in this lab. See the example below for creating a document in the car index.
\newpage
\begin{verbatim}
curl -XPOST "http://localhost:9200/car/toyota" -d'
  {
    "model": "Camry"
    "year": "2009"
    "color": "green"
  }'
\end{verbatim}

\section{Lab Instructions}

This lab will be separated into two separate portions: installing and configuring Elasticsearch, and inserting and deleting data in the cluster. \par
The Elasticsearch cluster for this lab will consist of three nodes, each running on an independent virtual machine with its own IP address (your IP addresses may differ):

\begin{table}[h!]
    \begin{center}
        \caption{\textbf{Elasticsearch cluster overview.}}
        \label{tab:table1}
        \begin{tabular}{l|c|r} % Alignment
            \toprule
            \textbf{Host Name} & \textbf{Role} & \textbf{IP Address}\\
            \midrule
            es-master-a & Master-Eligible Node & 192.168.128.4\\
            es-master-b & Master-Eligible Node & 192.168.128.7\\
            es-data-a & Data Node & 192.168.128.8\\
            \bottomrule
        \end{tabular}
    \end{center}
\end{table}

In production clusters, the master nodes generally do not do any data processing. However, for the purposes of this lab, the master nodes will process data using the \texttt{node.data: true} option in \texttt{/etc/elasticsearch/elasticsearch.yml}. 

\subsection{Prerequisites}

This lab will use Oracle VirtualBox as a hypervisor for three virtual machines. You are free to use different software if you prefer. You will also need 60 GB of free space on your drive and at least 8GB of memory.

\subsubsection{Downloads}

You will need Oracle VirtualBox installed on your computer.

\url{https://www.virtualbox.org/wiki/Downloads}
\linebreak

\noindent
You also need an image of Ubuntu 18.04 LTS Server.

\url{https://ubuntu.com/download/server}

\newpage
\subsection{Part 1 - Cluster Setup}

See Figure \ref{fig:arch} for the cluster architecture.

%\newpage
%\vfill

\begin{figure}[!h]
 \centering
 \includegraphics[width=0.6\linewidth]{DIS_Lab_Diagram2.png}
 \caption{\textbf{Elasticsearch cluster architecture.}}
 \label{fig:arch}
\end{figure}

%\vfill
%\clearpage

\subsubsection{Learning Objectives}

There are many reasons an organization may want to employ Elasticsearch. Many use what is known as the Elastic Stack, which is a collection of three separate software packages called Logstash, Elasticsearch, and Kibana, to aggregate log information from client machines. This project will introduce you to the concepts of virtualization, installing a Linux operating system, and installing software required to configure an Elasticsearch cluster. 

\subsubsection{Virtual Machine Creation}

\begin{enumerate}
    
    \item Create an initial virtual machine with these attributes (leave \textbf{Create a virtual hard disk now} selected):
        \begin{itemize}
            \item Name: \texttt{es-master-a}
            \item Type: Linux
            \item Version: Ubuntu (64-bit)
            \item Memory Size: 2560 MB
            \item File Size: 20.0 GB
        \end{itemize}
    \item Open the \textit{Preferences} window found in the \textit{File} menu and select \textit{Network} on the sidebar. Click the icon to add a NAT \footnote{Network Address Translation} Network.
    \item Set the Network Name to \textbf{dis} and the Network CIDR to \textbf{192.168.128.0/24}.
    \item Open the Settings window for your newly created VM.
    \item Choose \textit{Network} on the sidebar and select \textit{NAT Network} from the \textit{Attached to:} dropdown menu. Choose the \textbf{dis} NAT Network.
\end{enumerate}

\subsubsection{Install Ubuntu 18.04}

\begin{enumerate}
    \item Start the \texttt{es-master-a} virtual machine with the Start button.
    \item Choose the Ubuntu image file you downloaded previously when prompted.
    \item Choose the default options during the installer. When you get to the \textit{Profile setup} dialog, enter these details:
    \begin{itemize}
        \item Your name: \textbf{dis}
        \item Your server's name: \textbf{es-master-a}
        \item Pick a username: \textbf{dis}
        \item Choose a password: \textbf{dislab2020}
    \end{itemize}
    \item Do not enable ssh or install any suggested server snaps.
    \item Reboot when prompted.
\end{enumerate}

\subsubsection{Install Elasticsearch}

\begin{enumerate}
    \item Log in to the machine using the username and password you set during OS installation (\texttt{dis} and \texttt{dislab2020}).
    \item Update the apt repository: \texttt{sudo apt update}
    \item Install the \texttt{apt-transport-https} package necessary to access a repository over HTTPS: \texttt{sudo apt install apt-transport-https}
    \item Install OpenJDK 8: \texttt{sudo apt install openjdk-8-jdk}
    \item Import the Elasticsearch's repository’s GNU Privacy Guard (GPG) \footnote{GnuPG is a complete and free implementation of the OpenPGP standard as defined by RFC4880 (also known as PGP) \cite{gpg}} key with \texttt{wget}: 
    
    \texttt{wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -}
    
    You should see an OK message.
    \item Add the Elasticsearch repository: 
    
    \texttt{sudo sh -c 'echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" > /etc/apt/sources.list.d/elastic-7.x.list'}
    
    \item Use \texttt{cat} to verify the string \texttt{deb https://artifacts.elastic.co/packages/7.x/apt stable main} is present in the file \texttt{/etc/apt/sources.list.d/elastic-7.x.list}.
    
    \texttt{cat /etc/apt/sources.list.d/elastic-7.x.list}
    
    \item Update the repository information again with \texttt{sudo apt update} and then install Elasticsearch with \texttt{sudo apt install elasticsearch}
    
\end{enumerate}

\subsubsection{Initial Elasticsearch config}

\begin{enumerate}
    \item Open the Elasticsearch config file using the text editor of your choice. 
    
    \texttt{sudo vim /etc/elasticsearch/elasticsearch.yml}
    
    \item Note that most of the lines in this file are commented. Find the lines listed below, uncomment them if needed, and change their values as shown. The lines after the line break beginning with \texttt{node.} will need to be added yourself.
    
    \begin{verbatim}
cluster.name: discluster
node.name: es-master-a
# You need to run ifconfig to find your node's IP address
network.host: 192.168.128.4
discovery.seed_hosts: ["192.168.128.4", "192.168.128.7", "192.168.128.8"]
cluster.initial_master_nodes: ["192.168.128.4", "192.168.128.8"]

# These lines must be added
node.master: true 
node.voting_only: false 
node.data: true
node.ingest: true
node.ml: false 
xpack.ml.enabled: false
cluster.remote.connect: false 

    \end{verbatim}

\end{enumerate}


\subsubsection{Clone es-master-a}

\begin{enumerate}
    \item Power off \texttt{es-master-a}.
    \item Right click \texttt{es-master-a} in the sidebar. 
    \item Name the clones \texttt{es-master-b} and \texttt{es-data-a}.
    \item Use the \textit{Generate new MAC addresses for all network adapters} option when cloning. 
    \item Power on \texttt{es-master-a}, \texttt{es-master-b}, and \texttt{es-data-a}.
\end{enumerate}

\subsubsection{Configure Elasticsearch on the new nodes}

\begin{enumerate}
    \item You will notice the host name on both clones is still \texttt{es-master-a}. Run \texttt{sudo hostnamectl set-hostname es-master-b} and \texttt{sudo hostnamectl set-hostname es-data-a}.
    \item Run \texttt{sudo truncate -s 0 /etc/machine-id} on both clones and reboot.
    \item Make a note of the IP addresses on the clones after the reboot. Go to the cluster array lines in the Elasticsearch configuration file and make sure the IP addresses match the three VMs.
    \item The configuration file will still be present on the clones, but you must change the following lines to be unique for each node. 
    
    \begin{verbatim}
# es-master-b
node.name: es-master-b
# Use ifconfig to find IP address
network.host: 192.168.128.7

# es-data-a
node.name: es-data-a
# Use ifconfig to find IP address
network.host: 192.168.128.8
node.voting_only: true
    \end{verbatim}

    \item Start and enable the Elasticsearch service on each of the virtual machines:
    
    \begin{verbatim}
sudo systemctl enable elasticsearch
sudo systemctl start elasticsearch
    \end{verbatim}
    \item If there is no output, the service started successfully. You can check with \texttt{systemctl status elasticsearch}.

\end{enumerate}

\subsubsection{Submission}

\begin{enumerate}
    \item On each node, take screenshots with your name and student ID visible of the following commands to verify the cluster formation was successful. You will need to use the host's IP address on each VM, e.g. 192.168.128.4 on es-master-a, etc. See example below. Send these screenshots to your TA.
    \begin{verbatim}
curl -XGET "http://192.168.128.4:9200/_cluster/health?pretty"
curl -XGET "http://192.168.128.4:9200/_cat/nodes?pretty"
    \end{verbatim}
    \newpage
    \begin{figure}[!h]
    \centering
    \includegraphics[width=.9\linewidth]{submission_ss_2.png}
    \caption{\textbf{Example of lab submission screenshot - es-master-a.}}
    \label{fig:sub1}
    \end{figure}
    \begin{figure}[!h]
    \centering
    \includegraphics[width=.9\linewidth]{submission_ss_3.png}
    \caption{\textbf{Example of lab submission screenshot - es-master-b.}}
    \label{fig:sub2}
    \end{figure}
    \newpage
    \begin{figure}[!h]
    \centering
    \includegraphics[width=.9\linewidth]{submission_ss_4.png}
    \caption{\textbf{Example of lab submission screenshot - es-data-a.}}
    \label{fig:sub3}
    \end{figure}
    %\newpage
    \item Demonstrate the running cluster to your TA by running the following commands with successful return codes:
    \begin{verbatim}
sudo systemctl status elasticsearch
curl -XGET "http://192.168.128.4:9200/_cluster/health?pretty"
curl -XGET "http://192.168.128.4:9200/_cat/nodes?pretty"
    \end{verbatim}


\end{enumerate}



\subsection{Part 2 - Inserting and Deleting Elasticsearch Data}

\subsubsection{Learning Objectives}

\textbf{Resources}

\noindent
\url{https://www.w3resource.com/mongodb/nosql.php}
\linebreak

\noindent
The benefits of using a NoSQL database such as Elasticsearch are being free to store unstructured data. In this section, we will use the \texttt{curl} tool to perform HTTP requests such as \texttt{GET} and \texttt{POST} on our Elasticsearch cluster. We will also observe how nodes react when a member of the cluster is powered off unexpectedly. 
\newpage
\subsubsection{Create a New Index}

\textbf{Resources}

\noindent
\url{https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-create-index.html}
\linebreak

\noindent
To create the \texttt{car} index, send a file containing JSON data to Elasticsearch using \texttt{curl}. Create a \texttt{car.json} file in the text editor of your choice with the following contents:

\begin{verbatim}
{
  "settings" : {
    "index" : {
      "number_of_shards": 3,
      "number_of_replicas": 1
    }
  }
}
\end{verbatim}
\noindent
Create the index using \texttt{curl}:

\begin{verbatim}
curl -X PUT "192.168.128.4:9200/car?pretty" \
-H 'Content-Type: application/json' -d @car.json
\end{verbatim}

\noindent
Verify the index was created on \texttt{es-master-b} using \texttt{curl}:

\begin{verbatim}
curl -X GET "http://192.168.128.8:9200/_cat/indices
\end{verbatim}
\noindent
You should see an output line including the index name (car), the health state of the index (green), the shard count (3) and the document count (1). See Figure \ref{fig:catindex}. As new indices are created on single nodes in the cluster, they are replicated to the other members of the cluster automatically.

\begin{figure}[!h]
 \centering
 \includegraphics[width=\linewidth]{catindex.png}
 \caption{\textbf{Listing all indices in the cluster.}}
 \label{fig:catindex}
\end{figure}

\subsubsection{Add Documents to the car index}

\textbf{Resources}

\noindent
\url{https://www.elastic.co/guide/en/elasticsearch/reference/current/getting-started-index.html}
\linebreak

\noindent
Create a file named \texttt{inventory.json} with the following contents:

\begin{verbatim}
{"index":{"_id":"1"}}
{"make":"Toyota","model":"Camry","year":"1990","color":"green"}
{"index":{"_id":"2"}}
{"make":"Toyota","model":"Corolla","year":"2012","color":"blue"}
{"index":{"_id":"3"}}
{"make":"Toyota","model":"Celica","year":"2003","color":"white"}
{"index":{"_id":"4"}}
{"make":"Toyota","model":"Prius","year":"2016","color":"grey"}
{"index":{"_id":"5"}}
{"make":"Toyota","model":"Corolla","year":"2016","color":"white"}
{"index":{"_id":"6"}}
{"make":"Toyota","model":"Supra","year":"1994","color":"red"}
{"index":{"_id":"7"}}
{"make":"Toyota","model":"Yaris","year":"2014","color":"blue"}
{"index":{"_id":"8"}}
{"make":"Toyota","model":"Camry","year":"2017","color":"grey"}
{"index":{"_id":"9"}}
{"make":"Toyota","model":"Prius","year":"2014","color":"black"}
\end{verbatim}

\noindent
You can also download this file using \texttt{wget} in the terminal:

\begin{verbatim}
wget https://gitlab.com/spencerriner/dis_lab/-/raw/master/inventory.json
\end{verbatim}
\noindent
Perform a bulk upload of the inventory items with \texttt{curl}:

\noindent
\begin{verbatim}
curl -H "Content-Type: application/json" \
-X POST "http://192.168.128.4:9200/car/_bulk?pretty&refresh" \
--data-binary @inventory.json}
\end{verbatim}
 

\noindent
Search for one of the documents on \texttt{es-data-a} using \texttt{curl} and the id (1-9) to verify successful document creation. See Figure \ref{fig:catdoc9} for the expected output.

\begin{verbatim}
curl -X GET "http://192.168.128.7:9200/car/_doc/9?pretty"
\end{verbatim}
\newpage
\begin{figure}[!h]
 \centering
 \includegraphics[width=\linewidth]{catdoc9.png}
 \caption{\textbf{Listing document with id 9.}}
 \label{fig:catdoc9}
\end{figure}

\noindent
As shown, as documents are added, they are immediately replicated to the other nodes in the cluster. You can also use \texttt{curl} to search documents based on their properties. Use this curl command to find documents with the \texttt{color:white} property:

\begin{verbatim}
curl -X GET "http://192.168.128.4:9200/car/_search/?q=color:white&pretty"
\end{verbatim}

\begin{figure}[!h]
 \centering
 \includegraphics[width=\linewidth]{catwhite.png}
 \caption{\textbf{Listing documents with property color "white".}}
 \label{fig:catwhite}
\end{figure}

\subsubsection{Delete a Document}

\textbf{Resources}

\url{https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-delete.html}
\linebreak

You can also delete documents in an index with curl using their \texttt{id} property:

\begin{verbatim}
curl -X DELETE "http://192.168.128.4:9200/car/_doc/1?pretty"
\end{verbatim}

\noindent
You should see a confirmation as shown in Figure \ref{fig:deletedoc}.

\begin{figure}[!h]
 \centering
 \includegraphics[width=\linewidth]{deletedoc.png}
 \caption{\textbf{Deleting document 1.}}
 \label{fig:deletedoc}
\end{figure}

\subsubsection{Unexpected Node Shutdown}

One of the benefits of Elasticsearch is its ability to redistribute nodes if one of the cluster members is powered off unexpectedly. Find the current master node with \texttt{curl}:

\begin{verbatim}
curl -XGET "http://192.168.128.4:9200/_cat/master?pretty"
\end{verbatim}

\begin{figure}[!h]
 \centering
 \includegraphics[width=\linewidth]{catmaster.png}
 \caption{\textbf{Showing the master node of the cluster.}}
 \label{fig:catmaster}
\end{figure}

\noindent
Issue a \texttt{sudo poweroff} command to the current master node (in this case, \texttt{es-master-b}) then list all nodes on the other master-eligible node to verify the newly elected master node (it should have an asterisk next to its name). Then verify the cluster health to make sure that all 6 shards have been reallocated to the remaining nodes. See Figure \ref{fig:clusterhealth}.
\newpage
\begin{verbatim}
curl -XGET "http://192.168.128.4:9200/_cat/nodes?pretty"
curl -XGET "http://192.168.128.4:9200/_cluster/health?pretty"
\end{verbatim}

\begin{figure}[!h]
 \centering
 \includegraphics[width=\linewidth]{clusterhealth.png}
 \caption{\textbf{Showing the master node of the cluster.}}
 \label{fig:clusterhealth}
\end{figure}

\noindent
Power on the old master and run the cluster health command again to verify number of nodes is back to 3.

\subsubsection{Lab Submission}

Take screenshots of the results of these queries and submit to the TA.

\begin{enumerate}
 \item List all cars made in 2014
 \item List all Camry models
 \item List all grey cars
 \item Create a new car document with properties of your choice
 \item Delete the document with \texttt{id: 4}
\end{enumerate}
\noindent
Present a demo to the TA demonstrating the following:

\begin{enumerate}
 \item Show a documents properties by querying its \texttt{id}
 \item Search for a document using one of its properties
 \item Power off the master node, show the new master and a cluster health state of \texttt{green}
\end{enumerate}

\section{Conclusion}

You have now successfully created a distributed Elasticsearch cluster from scratch and demonstrated how it provides a high level of availability by tolerating sporadic node loss. Elasticsearch is highly scalable, as more nodes can be added to distribute the storage and compute needs of a growing dataset. 

\section{Further Study}

For more experience with Elasticsearch, you may try these additional projects.

\begin{flushleft}
\begin{enumerate}
    \item Install the Elastic Stack (Elasticsearch, Logstash, Kibana) on a cluster of VMs and visualize data using the Kibana web interface.
    \linebreak
    Tutorial: \url{https://www.digitalocean.com/community/tutorials/how-to-install-elasticsearch-logstash-and-kibana-elastic-stack-on-ubuntu-18-04}
    \item Send syslog messages from a Linux host to Logstash using rsyslog.
    \linebreak
    Tutorial: \url{https://www.digitalocean.com/community/tutorials/how-to-centralize-logs-with-rsyslog-logstash-and-elasticsearch-on-ubuntu-14-04}
    \item After installing the Elastic Stack, install Filebeat on the nodes to ingest data to the cluster.
    \linebreak
    Tutorial: \url{https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-getting-started.html}
\end{enumerate}
\end{flushleft}



%\section{Bibliography}

% Start bibliography
\nocite{*}
\bibliographystyle{ieeetr}
\bibliography{es_lab}

\end{document}
